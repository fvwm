#!/usr/bin/python

import sys


def exit_usage():
    sys.stderr.write('Usage: %s <curdesk> <mode> <min> <max>\n'
                     % sys.argv[0])
    sys.exit(1)

if len(sys.argv) < 5:
    exit_usage()

curdesk = int(sys.argv[1])
mode    = sys.argv[2]
min     = int(sys.argv[3])
max     = int(sys.argv[4])

if mode == 'inc':
    desk = curdesk + 1
else:
    desk = curdesk - 1

if desk > max:
    desk = min
elif desk < min:
    desk = max

sys.stdout.write('InfoStoreAdd target_desk %d\n' % desk)
sys.exit(0)
