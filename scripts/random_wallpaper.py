#!/usr/bin/python
# Reads a given directory and set a random
# wallpaper using the images on that dir.

import subprocess
import os
import os.path
import sys
import random

if len(sys.argv) < 2:
    sys.stderr.write('Usage: %s <wallpaper_dir>\n'%(sys.argv[0]))
    sys.exit(-1)

d = os.path.expandvars(sys.argv[1])

sys.stderr.write('Trying directory %s.\n'%d)
if not os.path.isdir(d):
    sys.stderr.write('Path %s is not a directory, aborting.\n'%d)
    sys.exit(-1)

files = os.listdir(d)
random.seed()
try:
    f = random.choice(files)
except IndexError:
    sys.stderr.write('No files in dir %s, aborting.\n'%d)
    sys.exit(-1)

sys.stderr.write('Selected file %s.\n'%f)

try:
    p = subprocess.Popen('xdpyinfo', stdout = subprocess.PIPE)
    for line in p.stdout:
        if 'dimensions' in line:
            res = line.split()[1]
            break
    sys.stderr.write('X screen resolution is %s.\n'%res)
except (OSError, IndexError, NameError):
    sys.stderr.write('Error getting X screen resolution, aborting.\n')
    sys.exit(-1)

if not os.path.isdir(os.path.expandvars('${FVWM_USERDIR}/tmp/')):
    try:
        os.mkdir(os.path.expandvars('${FVWM_USERDIR}/tmp/'))
    except OSError:
        sys.stderr.write('Can\'t create the tmp directory.')
        sys.exit(-1)
try:
    subprocess.check_call(['convert', os.path.join(d, f), '-resize', res, os.path.expandvars('${FVWM_USERDIR}/tmp/current_wall.png')])
except (subprocess.CalledProcessError, OSError):
    sys.stderr.write('Error resizing the wallpaper. Is Imagemagick installed?\n')
    sys.exit(-1)

try:
    subprocess.check_call(['feh', '--bg-center', os.path.expandvars('${FVWM_USERDIR}/tmp/current_wall.png')])
except (subprocess.CalledProcessError, OSError):
    sys.stderr.write('Error setting the wallpaper. Is feh installed?\n')
    sys.exit(-1)

sys.exit(0)
