#!/usr/bin/python

# switch to desk and page

import sys


def exit_usage():
    sys.stderr.write('Usage: %s <desknum> <pagenum> <curpage>' % sys.argv[0])
    sys.exit(1)

if len(sys.argv) < 4:
    exit_usage()

desknum = int(sys.argv[1])
pagenum = sys.argv[2]
curpage = int(sys.argv[3])

if pagenum[0] == '$':
    page = int(curpage)
else:
    page = int(pagenum)

sys.stdout.write('GotoDeskAndPage %d 0 %d\n' % (desknum, page))
sys.exit(0)
