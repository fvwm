#!/bin/bash
#i=$(cat ${FVWM_USERDIR}/config-options/desktop_size)
num_pages=$1
shift 1
i=1x${num_pages}
j=${i}

i=${i%x*}
j=${j#*x}

a=0
b=0

while [[ ${a} -lt ${i} ]]
do 
	while [[ ${b} -lt ${j} ]]
	do
		echo + "\"${@} $a,$b\"" Pick MoveToPage $a $b
		b=$((${b} + 1))
	done
	a=$((${a} + 1))
	b=0
done
