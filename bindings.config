###########################
# Keyboard/mouse bindings #
###########################
#
# /usr/include/X11/keysymdef.h for keys
#

# main menus
Mouse 1 R A Menu menu_RootOps
Mouse 2 R A Menu menu_WindowOps
Key C A 4 Menu menu_fvwmConfig Root 10p 10p

# Contextual menus
Key D A 4 Menu menu_WindowSendToPage Window 10p 10p
Key R A 4 Menu menu_WindowOps Window 10p 10p

# Close window
Key F4 A M Close

# Mouse bindings for windows
Mouse 1 W M FvwmWindowMove
Mouse (mplayer*) 1 W A FvwmWindowMove
Mouse (FvwmIdent) 1 W A FvwmWindowMove
Mouse (mplayer*) 3 W A FvwmWindowResize
Mouse 1 T N FvwmWindowTitleClick
Mouse 4 T N WindowShade true
Mouse 5 T N WindowShade false

# click on window title
Mouse 1 2 A FvwmWindowClose
Mouse 1 4 A FvwmWindowMaximize 100 100
Mouse 2 2 A FvwmWindowDestroy
Mouse 2 4 A Maximize 100 0
Mouse 3 4 A Maximize 0 100
Mouse 1 1 A Menu menu_WindowOps

# Key bindings for windows
Key M A 4 FvwmWindowMaximizeNoClick
Key G A 4 Resize w+10 w+10
Key G A S4 Resize w-10 w-10
Key S A 4 Pick Resize warptoborder

# Move windows with keys
Key Down WFS 4M AnimatedMove w+0 w+5 Warp
Key Up WFS 4M AnimatedMove w+0p w-5 Warp
Key Left WFS 4M AnimatedMove w-5 w+0 Warp
Key Right WFS 4M AnimatedMove w+5 w+0 Warp

Key Up WFS MC4 Pick AnimatedMove screen c keep 0p Warp
Key Left WFS MC4 Pick AnimatedMove screen c 0p keep Warp
Key Right WFS MC4 Pick AnimatedMove screen c -0p keep Warp
Key Down WFS MC4 Pick AnimatedMove screen c keep -0p Warp

Key Up WFS 4C Resize w+0 w-10
Key Down WFS 4C Resize w+0 w+10
Key Left WFS 4C Resize w-10 w+0
Key Right WFS 4C Resize w+10 w+0

# Move windows across pages
Key plus A 4 MoveToPage 0p, +1w
Key KP_Add A 4 MoveToPage 0p, +1w
Key minus A 4 MoveToPage 0p, -1w
Key KP_Subtract A 4 MoveToPage 0p, -1w


# Switching between windows
Key Tab A M WindowList Root c c currentDesk, \
	NoGeometry, NoCurrentDeskTitle, CurrentAtEnd, \
	NoNumInDeskTitle
Key Tab A 4 Next (CurrentPage, !Gkrellm, !FvwmPager) next_window_in_page
Key Tab A CM Next (CurrentPage, !Gkrellm, !FvwmPager) next_window_in_page

# Do something interesting with the extra mouse buttons
Silent Mouse 6 T N Resize br w-10 w-10
Silent Mouse 7 T N Resize br w+10 w+10
Silent Mouse 8 W N FvwmWindowMove
Silent Mouse 9 W N FvwmWindowResize

# move across desktops with arrows
Key Left  A 4 DeskPrev
Key H     A 4 DeskPrev
Key Right A 4 DeskNext
Key L     A 4 DeskNext

# select a desktop with win+<f*>
Piperead 'for i in `seq 0 11`; do j=$(($i+1)); echo Key F$j A 4 DeskGoto $i; done'

# name desktops
Key n A 4 FormDeskName

# Move across pages with up/down arrows, mouse scroll or j/k
Key   Up A 4 GotoPage  0p -1p
Key   K  A 4 GotoPage 0p -1p
Mouse 4  A 4 GotoPage 0p -1p
Key   Down A 4 GotoPage  0p +1p
Key   J    A 4 GotoPage 0p +1p
Mouse 5    A 4 GotoPage 0p +1p

# select a page directly with a number (0-9) or shift + number (10-19)
Piperead 'for i in `seq 0 8`; do j=$(($i+1)); echo Key $j A 4 GotoPage 0 $i; done'
Key 0 A 4 GotoPage 0 9
Piperead 'for i in `seq 10 18`; do j=$(($i-9)); echo Key $j A S4 GotoPage 0 $i; done'
Key 0 A S4 GotoPage 0 19

# Kill
Test (x xkill) Key Escape A M Exec xkill

# Restart FVWM
Key Delete A CM Restart

# Lock screen
Key Pause A 4 Exec $[infostore.lock]

####################
# Launching apps   #
####################

# FvwmConsole
Key F A 4 FvwmConsole -terminal $[infostore.terminal]

# Launch preferred applications
Key T A 4 Exec exec $[infostore.terminal]
Key V A 4 Exec exec $[infostore.terminal] -e alsamixer
Key Escape A 4 Exec exec $[infostore.terminal] -e htop

# screenshot
Key Print A 4 Exec exec import -window root $[HOME]/screen-$(date +%Y%m%d_%H%M%S).png

#wallpaper
Key W A 4 RandomWall

#MPD control
Key grave A 4 MPDControl toggle
Key B     A 4 MPDControl next
Key Z     A 4 MPDControl prev
Key P     A 4 MPDControl play
Key Page_Up A 4 MPDControl volume +1
Key Page_Down A 4 MPDControl volume -1
